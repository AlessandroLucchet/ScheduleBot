﻿using Microsoft.Extensions.Logging;
using Quartz;
using ScheduleBot.Properties;
using System;
using System.Threading.Tasks;

namespace ScheduleBot
{
    class ScheduledJob : IJob
	{
		//region Job parameters
		public static String BotName = "BotName";
		public static String ChatId = "ChatId";
		public static String Command = "Command";
		public static String Format = "Format";
		//endregion

		public Task Execute(IJobExecutionContext context)
		{
			JobDataMap dataMap = context.JobDetail.JobDataMap;

			// Log scheduled action
			Worker.Logger.Log(LogLevel.Information, string.Format(Strings.sending_scheduled, dataMap.GetString(BotName), dataMap.GetString(Command), dataMap.GetLong(ChatId)));

			// Launch command
			Logic.OnCommand(
				botName: dataMap.GetString(BotName), 
				chatId: dataMap.GetLong(ChatId), 
				command: dataMap.GetString(Command), 
				scheduledMessageReplyFormat: dataMap.GetString(Format),
				jobKey: context.JobDetail.Key
				);

			return null;
		}
	}
}
