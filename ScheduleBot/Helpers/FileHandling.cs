﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ScheduleBot.Helpers
{
    class FileHandling
    {
        public static string AppDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ScheduleBot");
        private static string GetApplicationDirectory()
        {
            Directory.CreateDirectory(AppDataPath);
            return AppDataPath;
        }

        private static string GetLogDirectory()
        {
            return Directory.CreateDirectory(Path.Combine(GetApplicationDirectory(), "Log")).FullName;
        }

        public static String GetLogFilePath(String LogName)
        {
            return Path.Combine(GetLogDirectory(), LogName);
        }
    }
}
