﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace ScheduleBot.Models
{
    class SourceDictionary
    {
        /// <summary>
        /// This function extrscts in a dictionary the columns A and B of the Google Sheet described in the source
        /// </summary>
        /// <param name="source">The source of the data</param>
        /// <returns></returns>
        public static Dictionary<string,string> GetFromSource(Source source, String GoogleSheetApiKey)
        {
            GoogleSheet googleSheet;
            Dictionary<string, string> sourceDictionary = new Dictionary<string, string>();

            try
            {
                // Download and parse Google Sheet from json format
                using (WebClient client = new WebClient())
                {
                    string json = client.DownloadString(source.GetJsonUrl(GoogleSheetApiKey));
                    googleSheet = GoogleSheet.Parse(json);
                }

                // Compose a dictionary from column A and B
                foreach (String[] row in googleSheet.values)
                    if(row.Length == 2)
                        sourceDictionary.Add(row[0],row[1]);
            }
            catch(Exception e)
            {
                Worker.Logger.Log(LogLevel.Error, e.Message);
            }
            return sourceDictionary;
        }
    }
}
