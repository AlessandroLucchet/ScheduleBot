﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Quartz;
using ScheduleBot.Helpers;
using ScheduleBot.Models;
using ScheduleBot.Properties;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;

namespace ScheduleBot
{
    class Logic
    {
        public static async void OnCommand(string botName, long chatId, string command, Message message = null, string scheduledMessageReplyFormat = null, JobKey jobKey = null)
        {
            bool isAdmin = message != null && Worker.Config.IsAdmin(message.Chat.Username);
            bool isAllowed = isAdmin || Worker.Config.Bots[botName].AllowedChatIds == null || Worker.Config.Bots[botName].AllowedChatIds.ContainsKey(chatId.ToString());

            try
            {
                switch (command)
                {
                    case Command.Start:
                        // Reply with the start message of the bot
                        if (Worker.Config.Bots[botName].StartMessage != null && message != null)
                            await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Worker.Config.Bots[botName].StartMessage, message.Chat.FirstName));
                        // If is admin, send also available commands
                        if (isAdmin)
                        {
                            StringBuilder stringBuilder = new StringBuilder();
                            foreach (var info in typeof(Command).GetFields()/*.OrderBy(command => command.Name)*/)
                                stringBuilder.AppendLine(info.Name + ": " + (string)info.GetRawConstantValue());
                            await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Strings.available_commands, stringBuilder.ToString()).Replace("\\n", "\n"));
                        }
                        return;

                    case Command.ChatId:
                        // Reply with this chat Id
                        await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Strings.chat_id, chatId));
                        return;

                    case Command.BotInfo:
                        // Reply with bot info
                        await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Strings.bot_info_details, Assembly.GetEntryAssembly().GetName().Version.ToString()).Replace("\\n", "\n"));
                        return;

                    case Command.GetTriggers:
                        if (isAdmin)
                            await Worker.botClients[botName].SendTextMessageAsync(chatId, JsonConvert.SerializeObject(Worker.Config.Bots[botName].Triggers, Formatting.Indented));
                        return;

                    case Command.GetAliases:
                        if (isAdmin)
                            await Worker.botClients[botName].SendTextMessageAsync(chatId, JsonConvert.SerializeObject(Worker.Config.Bots[botName].CommandAliases, Formatting.Indented));
                        return;

                    case Command.GetAllowedChats:
                        if (isAdmin)
                            await Worker.botClients[botName].SendTextMessageAsync(chatId, JsonConvert.SerializeObject(Worker.Config.Bots[botName].AllowedChatIds, Formatting.Indented));
                        return;
                }

                // Regexs
                List<String> regexs = new List<string> { Command.SetTriggersRegex, Command.GetSourceRegex, Command.AddAllowedChatIdRegex, Command.RemoveAllowedChatIdRegex, Command.GetScheduleRegex, Command.SetAliasesRegex };
                foreach (string regex in regexs)
                {
                    Match regexMatch = Regex.Match(command, regex);
                    if (regexMatch.Success)
                    {
                        // Public commands, for allowed chats
                        if (isAllowed)
                            switch (regex)
                            {
                                case Command.GetSourceRegex:
                                    await Worker.botClients[botName].SendTextMessageAsync(chatId, Worker.Config.Bots[botName].Sources[regexMatch.Groups[1].Value].GetUrl());
                                    return;

                                case Command.GetScheduleRegex:
                                    // Symulate typing
                                    await Worker.botClients[botName].SendChatActionAsync(chatId, Telegram.Bot.Types.Enums.ChatAction.Typing);

                                    // Get schedule data
                                    Dictionary<string, string> dictionary = SourceDictionary.GetFromSource(Worker.Config.Bots[botName].Sources[regexMatch.Groups[1].Value], Worker.Config.GoogleSheetApiKey);

                                    // Get reference date
                                    DateTime referenceDate = DateTime.Now;
                                    switch (regexMatch.Groups[2].Value)
                                    {
                                        case "today":
                                            referenceDate = DateTime.Now;
                                            break;

                                        case "weekstart":
                                            referenceDate = DateUtil.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
                                            break;

                                        case "monthstart":
                                            referenceDate = DateUtil.StartOfMonth(DateTime.Now);
                                            break;
                                    }

                                    // Eventually, add/subtract to the reference date
                                    if (regexMatch.Groups[3].Success)
                                    {
                                        int sign = (regexMatch.Groups[4].Value.Equals("-") ? -1 : 1);
                                        int number = Int32.Parse(regexMatch.Groups[5].Value);
                                        switch (regexMatch.Groups[6].Value)
                                        {
                                            case "d":
                                                referenceDate = referenceDate.AddDays(sign * number).Date;
                                                break;
                                            case "w":
                                                referenceDate = referenceDate.AddDays(sign * 7 * number).Date;
                                                break;
                                            case "m":
                                                referenceDate = referenceDate.AddMonths(sign * number).Date;
                                                break;
                                        }
                                    }

                                    // Result
                                    string referenceDateString = referenceDate.ToString(Worker.Config.Bots[botName].Sources[regexMatch.Groups[1].Value].DateFormat);
                                    bool referenceFound = dictionary.ContainsKey(referenceDateString);
                                    Worker.Logger.Log(referenceFound ? LogLevel.Information : LogLevel.Warning, string.Format(referenceFound ? Strings.reference_found : Strings.reference_not_found, referenceDateString, regexMatch.Groups[1].Value, botName));
                                    if (referenceFound)
                                    {
                                        string scheduledMessage = scheduledMessageReplyFormat == null ? dictionary[referenceDateString] : string.Format(scheduledMessageReplyFormat, dictionary[referenceDateString]).Replace("\\n", "\n");
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, scheduledMessage);
                                    }
                                    return;
                            }

                        // Regex for admins only
                        if (isAdmin)
                            switch (regex)
                            {
                                case Command.SetTriggersRegex:
                                    try
                                    {
                                        // Try to parse the incoming value
                                        List<Trigger> newTriggerList = JsonConvert.DeserializeObject<List<Trigger>>(regexMatch.Groups[1].Value);

                                        // Try to use the cron expression (validity check)
                                        foreach(Trigger trigger in newTriggerList)
                                        {
                                            ITrigger tr = TriggerBuilder.Create()
                                            .WithCronSchedule(trigger.Cron)
                                            .Build();
                                        }

                                        // Reload triggers
                                        Worker.ScheduleJobs(botName, newTriggerList);

                                        // If it's ok, change the existing triggers
                                        Worker.Config.Bots[botName].Triggers = newTriggerList;

                                        // Save existing configuration
                                        Worker.Config.Save();

                                        // Feedback
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.done);
                                    }
                                    catch (JsonException exception)
                                    {
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.invalid_trigger_list);
                                        Worker.Logger.Log(LogLevel.Error, exception.Message);
                                    }
                                    catch(System.FormatException exception)
                                    {
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.invalid_cron_expression);
                                        Worker.Logger.Log(LogLevel.Error, exception.Message);
                                    }
                                    return;

                                case Command.SetAliasesRegex:
                                    try
                                    {
                                        // Try to parse the incoming value
                                        Dictionary<string, string> newAliasesList = JsonConvert.DeserializeObject<Dictionary<string, string>>(regexMatch.Groups[1].Value);

                                        // If it's ok, change the existing triggers
                                        Worker.Config.Bots[botName].CommandAliases = newAliasesList;

                                        // Save existing configuration
                                        Worker.Config.Save();

                                        // Feedback
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.done);
                                    }
                                    catch (JsonException exception)
                                    {
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.invalid_alias_list);
                                        Worker.Logger.Log(LogLevel.Error, exception.Message);
                                    }
                                    return;

                                case Command.AddAllowedChatIdRegex:
                                    if (Worker.Config.Bots[botName].AllowedChatIds == null)
                                        Worker.Config.Bots[botName].AllowedChatIds = new Dictionary<string, string>();

                                    try
                                    {
                                        // Get chat info for this chat
                                        var chat = Worker.botClients[botName].GetChatAsync(Int32.Parse(regexMatch.Groups[1].Value)).Result;

                                        // Add the chat to the allowed ones
                                        String name = "";
                                        switch (chat.Type)
                                        {
                                            case Telegram.Bot.Types.Enums.ChatType.Group:
                                            case Telegram.Bot.Types.Enums.ChatType.Supergroup:
                                            case Telegram.Bot.Types.Enums.ChatType.Channel:
                                                name = chat.Title;
                                                break;
                                            case Telegram.Bot.Types.Enums.ChatType.Private:
                                                name = (chat.FirstName + " " + chat.LastName).Trim();
                                                break;
                                        }
                                        Worker.Config.Bots[botName].AllowedChatIds.Remove(regexMatch.Groups[1].Value);
                                        Worker.Config.Bots[botName].AllowedChatIds.Add(regexMatch.Groups[1].Value, name);

                                        // Save changes
                                        Worker.Config.Save();

                                        // Feedback
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Strings.add_allowed, name));
                                    }
                                    catch (Exception cnfe)
                                    {
                                        // Feedback
                                        await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.chat_not_found);
                                        Worker.Logger.Log(LogLevel.Warning, cnfe.Message);
                                    }
                                    return;

                                case Command.RemoveAllowedChatIdRegex:
                                    if (Worker.Config.Bots[botName].AllowedChatIds != null)
                                    {
                                        if (Worker.Config.Bots[botName].AllowedChatIds.ContainsKey(regexMatch.Groups[1].Value))
                                        {
                                            // Feedback
                                            await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Strings.remove_allowed, Worker.Config.Bots[botName].AllowedChatIds[regexMatch.Groups[1].Value]));

                                            // Remove
                                            Worker.Config.Bots[botName].AllowedChatIds.Remove(regexMatch.Groups[1].Value);

                                            // Save changes
                                            Worker.Config.Save();

                                            // Feedback
                                            await Worker.botClients[botName].SendTextMessageAsync(chatId, Strings.done);
                                        }
                                    }
                                    return;
                            }

                        if (!isAllowed)
                            // Inform that you arent allowed
                            await Worker.botClients[botName].SendTextMessageAsync(chatId, string.Format(Strings.not_allowed, chatId));

                    }
                }
            }
            catch(HttpRequestException httpException)
            {
                // If something goes wrong, if this command was launched by a job, then add it to the pending jobs
                if (jobKey != null && !Worker.pendingJobs.Contains(jobKey) )
                { 
                    Worker.pendingJobs.Add(jobKey);
                    Worker.Logger.LogError(String.Format(Strings.job_queued,httpException.Message));
                }
            }

            // Fetch aliases
            foreach (KeyValuePair<string, string> alias in Worker.Config.Bots[botName].CommandAliases)
                if (command.StartsWith(alias.Key))
                    OnCommand(botName, chatId, alias.Value, message, scheduledMessageReplyFormat, jobKey);
        }
    }
}
