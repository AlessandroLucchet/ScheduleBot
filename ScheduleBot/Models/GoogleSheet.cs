﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ScheduleBot.Models
{
    public class GoogleSheet
    {
        public String range { get; set; }
        public String majorDimension { get; set; }
        public String[][] values { get; set; }

        public static GoogleSheet Parse(String json)
        {
            return JsonConvert.DeserializeObject<GoogleSheet>(json);
        }
    }
}
