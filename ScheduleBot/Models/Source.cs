﻿namespace ScheduleBot.Models
{
    /// <summary>
    /// This is a source of information
    /// It's get from a published Google Sheet
    /// </summary>
    public class Source
    {
        /// <summary>
        /// The Google Sheet Id. You can find it in the Google Sheet URL
        /// </summary>
        public string Id { get; set; }
        public string Range { get; set; }
        public string DateFormat { get; set; }

        public string GetJsonUrl(string GoogleSheetApiKey)
        {
            return string.Format("https://sheets.googleapis.com/v4/spreadsheets/{0}/values/{1}?majorDimension=ROWS&key={2}", Id, Range, GoogleSheetApiKey);
        }

        public string GetUrl()
        {
            return string.Format("https://docs.google.com/spreadsheets/d/{0}",Id);
        }
    }
}
