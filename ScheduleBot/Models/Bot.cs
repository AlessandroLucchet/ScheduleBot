﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleBot.Models
{
    public class Bot
    {
        public string Token { get; set; }
        public string StartMessage { get; set; }
        public Dictionary<String, Source> Sources { get; set; }
        public List<Trigger> Triggers { get; set; }
        public Dictionary<String, String> AllowedChatIds { get; set; }
        public Dictionary<String, String> CommandAliases { get; set; }
    }
}
