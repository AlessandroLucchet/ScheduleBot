﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleBot.Models
{
    public class Trigger
    {
        /// <summary>
        /// When the trigger should be scheduled
        /// Example: "0 7 ? * 7" (every saturday at 7am)
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// Chat id where the message should be sent
        /// </summary>
        public long ChatId { get; set; }
        
        /// <summary>
        /// Command or alias that would be executed
        /// </summary>
        public string Command { get; set; }

        /// <summary>
        /// Format of the message (optional)
        /// Example: "Here is your schedule: {0}"
        /// </summary>
        public string Format { get; set; }
    }
}