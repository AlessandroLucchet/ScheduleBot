﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScheduleBot.Properties {
    using System;
    
    
    /// <summary>
    ///   Classe di risorse fortemente tipizzata per la ricerca di stringhe localizzate e così via.
    /// </summary>
    // Questa classe è stata generata automaticamente dalla classe StronglyTypedResourceBuilder.
    // tramite uno strumento quale ResGen o Visual Studio.
    // Per aggiungere o rimuovere un membro, modificare il file con estensione ResX ed eseguire nuovamente ResGen
    // con l'opzione /str oppure ricompilare il progetto VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Restituisce l'istanza di ResourceManager nella cache utilizzata da questa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ScheduleBot.Properties.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Esegue l'override della proprietà CurrentUICulture del thread corrente per tutte le
        ///   ricerche di risorse eseguite utilizzando questa classe di risorse fortemente tipizzata.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a {0} has been added to the allowed chats.
        /// </summary>
        internal static string add_allowed {
            get {
                return ResourceManager.GetString("add_allowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Available commands:\n{0}.
        /// </summary>
        internal static string available_commands {
            get {
                return ResourceManager.GetString("available_commands", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a ScheduleBot version: {0}\nRepository and infos: https://gitlab.com/AlessandroLucchet/ScheduleBot.
        /// </summary>
        internal static string bot_info_details {
            get {
                return ResourceManager.GetString("bot_info_details", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Started ScheduleBot v{0}.
        /// </summary>
        internal static string bot_started {
            get {
                return ResourceManager.GetString("bot_started", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a This chat ID is: {0}.
        /// </summary>
        internal static string chat_id {
            get {
                return ResourceManager.GetString("chat_id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Chat not found.
        /// </summary>
        internal static string chat_not_found {
            get {
                return ResourceManager.GetString("chat_not_found", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Configuration file &quot;config.json&quot; not found.
        /// </summary>
        internal static string config_not_found {
            get {
                return ResourceManager.GetString("config_not_found", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Done.
        /// </summary>
        internal static string done {
            get {
                return ResourceManager.GetString("done", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Got back online. Now I launch queued jobs.
        /// </summary>
        internal static string got_back_online {
            get {
                return ResourceManager.GetString("got_back_online", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a That&apos;s not an alias list.
        /// </summary>
        internal static string invalid_alias_list {
            get {
                return ResourceManager.GetString("invalid_alias_list", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Invalid cron expression in one of your triggers.
        /// </summary>
        internal static string invalid_cron_expression {
            get {
                return ResourceManager.GetString("invalid_cron_expression", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a 🔑 Invalid token! Please, verify that in the &quot;config.json&quot; file there is the correct value on &quot;BotToken&quot;.
        /// </summary>
        internal static string invalid_token {
            get {
                return ResourceManager.GetString("invalid_token", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a That&apos;s not a trigger list.
        /// </summary>
        internal static string invalid_trigger_list {
            get {
                return ResourceManager.GetString("invalid_trigger_list", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Error while processing a command ({0}). This job has been added to the queue.
        /// </summary>
        internal static string job_queued {
            get {
                return ResourceManager.GetString("job_queued", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a You are not allowed to see contents from this bot. If you think you should, send to the bot creator this ID: {0}.
        /// </summary>
        internal static string not_allowed {
            get {
                return ResourceManager.GetString("not_allowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a I&apos;m not online yet. Retrying in a couple of seconds....
        /// </summary>
        internal static string not_online_yet {
            get {
                return ResourceManager.GetString("not_online_yet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Found reference date {0} in the source {1} of bot {2}.
        /// </summary>
        internal static string reference_found {
            get {
                return ResourceManager.GetString("reference_found", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Not found reference date {0} in the source {1} of bot {2}.
        /// </summary>
        internal static string reference_not_found {
            get {
                return ResourceManager.GetString("reference_not_found", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a {0} has been removed from the allowed chats.
        /// </summary>
        internal static string remove_allowed {
            get {
                return ResourceManager.GetString("remove_allowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Scheduled job for bot {0} at: {1}.
        /// </summary>
        internal static string scheduled_job {
            get {
                return ResourceManager.GetString("scheduled_job", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Bot {0} will send command {1} to chat id {2}.
        /// </summary>
        internal static string sending_scheduled {
            get {
                return ResourceManager.GetString("sending_scheduled", resourceCulture);
            }
        }
    }
}
